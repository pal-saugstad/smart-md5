#!/usr/bin/python3

import os
import hashlib

# Pal Saugstad, 2021-08-09 (pal@saugstad.net)


def calc(filename):
    '''
    Make a signature == md5sum for short files, and md5sum for some parts (3 x 1 Mbyte) of big files
    '''
    chunk_size = 1024*1024
    try:
        size = os.stat(filename).st_size
    except FileNotFoundError:
        return("NO_SUCH_FILE 0 "+filename)

    if size == 0:
        return("d41d8cd98f00b204e9800998ecf8427e 0 "+filename)

    chunks = size // chunk_size
    try:
        m = hashlib.md5()
        with open(filename, 'rb') as fp:
            if (chunks < 4):
                m.update(fp.read())
            else:
                m.update(fp.read(chunk_size))
                fp.seek((chunks//2)*chunk_size)
                m.update(fp.read(chunk_size))
                fp.seek((chunks-1)*chunk_size)
                m.update(fp.read(chunk_size))
                m.update(str(size).encode('UTF-8'))
        return (m.hexdigest()+' '+str(size)+' '+filename)
    except OSError:
        return ("OSError_CALC " + str(size) + ' ' + filename)

    return ("UNKNOWN_CALC 0 " + filename)


def plain_md5(filename):
    chunk_size = 1024*1024
    try:
        m = hashlib.md5()
        with open(filename, 'rb') as fp:
            chunk = fp.read(chunk_size)
            while len(chunk) > 0:
                m.update(chunk)
                chunk = fp.read(chunk_size)
        return (m.hexdigest()+'  '+filename)
    except OSError:
        return ("OSError_PLAIN_MD5  " + filename)


def scan_files(name, small_lim_k):
    '''
    A generator based on the list of file given by the 'name'.
    This file contains a sorted list of files, sorted by the signatures (hash values)
    scan_files() generates new unique enties from that list.
    If the same entry (same hash) is discovered again, it just keeps track of the
    total amount of bytes it has discovered in superfluous files
    If no more files in list, repeat the 'higher than any' hash 'zzzz', and
    report the internal variables tot_size, tot_reduction and tot_uniq_files.
    '''
    psum = '0'
    tot_size = 0
    tot_reduction = 0
    tot_uniq_files = 0
    with open(name, 'r') as fp:
        line = fp.readline()
        while line:
            fsum, fs, order, fname = line.strip().split(' ', 3)
            if len(fsum) == 32:
                tot_size += int(fs)
                if fsum != psum:
                    tot_uniq_files += 1
                    yield fsum, int(fs), order, fname
                    psum = fsum
                else:
                    if int(fs) > small_lim_k:
                        tot_reduction += int(fs)
            line = fp.readline()
    while True:
        # send tot_size instead of fs and tot_reduction instead of order
        yield 'zzzz', tot_size, str(tot_reduction), str(tot_uniq_files)


'''
Keep track of hash pairs which has been reported
'''
hash_vals = []


def eval_file(right, wsum, name, dest):
    '''
    Check if there is a file with the same name on the other disk,
    and figure out if thee content is different or the same.
    Don't report the pair if already reported
    (based on registrations in the hash_val list)
    '''
    os.chdir(dest)
    if os.path.isfile(name):
        rsum, sz, name = calc(name).strip().split(' ', 2)
        ret = 0 if rsum == wsum else 1
        if ret:
            if right:
                new_hash = rsum[:11]+wsum[:11]
            else:
                new_hash = wsum[:11]+rsum[:11]
            if new_hash in hash_vals:
                ret = 2
            else:
                hash_vals.append(new_hash)
        return ret, rsum
    return -1, ' ' * 15 + '-' + ' ' * 16


class FileAttributes:
    '''
    Keep track of file attributes of current file,
    and accumulated values regarding statitics of files
    '''
    def __init__(self, addr, get_file, small_lim):
        self.known = 1
        self.accu_sum = 0
        self.abs = addr
        self.get_file = scan_files(get_file, small_lim * 1024)  # Activate the file report producing generator
        self.files = 0
        self.tot = 0
        self.tot_uniq_files = 0
        self.red = 0
        self.sum = '0'
        self.size = 0
        self.order = '0'
        self.name = 'name'

    def reg(self, vals):
        self.sum, self.size, self.order, self.name = vals
        if self.sum == 'zzzz':
            self.tot = self.size
            self.red = int(self.order)
            self.tot_uniq_files = int(self.name)
            return 1
        return 0

    def add_size(self):
        self.accu_sum += self.size


def check_sync(args):
    '''
    Produce data for detailed statitics regarding differences in file structure
    of Disk1 and Disk2

    Arguments:
       - Folder where the results shall be stored
       - Make a cross-duplicate file (y/n)
       - Limit in KB regarding duplicate check
       - Absolute pointer to Disk1 folder
       - Filename of file with sorted list of files on Disk1 (sorted by signatures)
       - Absolute pointer to Disk2 folder
       - Filename of file with sorted list of files on Disk2 (sorted by signatures)
    '''
    out_dir = args[0]
    args.pop(0)
    cross = args[0]
    args.pop(0)
    small_lim = int(args[0])
    args.pop(0)

    '''
    Store file attributes of Disk1 in attribs[0] and
          file attributes of Disk2 in attribs[1]
    '''
    attribs = [FileAttributes(args[0], args[1], small_lim),
               FileAttributes(args[2], args[3], small_lim)]

    # Delete earlier report files for both disks
    for abs_base in [args[0], args[2]]:
        for name in ['sync', 'put', 'get', 'cross-duplicates']:
            fn = os.path.join(abs_base, out_dir, name)
            if os.path.isfile(fn):
                os.unlink(fn)

    abs_base = args[0]
    report_file = os.path.join(out_dir, 'sync')
    cross_file = os.path.join(out_dir, 'cross-duplicates')
    put_files = os.path.join(out_dir, 'put')
    get_files = os.path.join(out_dir, 'get')

    bdiff = 0
    bsame = 0
    end = 0
    with open(cross_file, "w") as cfp, open(report_file, "w") as rfp, open(put_files, "w") as put_fp, open(get_files, "w") as get_fp:
        rfp.write("Disk1='{}' (This)\n".format(attribs[0].abs))
        rfp.write("Disk2='{}'\n".format(attribs[1].abs))
        rfp.write("---------------------------- Statistics ---------------------------\n\n")
        while True:
            # Goto next unique file for the Disk(s) with already analyzed file
            for i in [0, 1]:
                f = attribs[i]
                if f.known:
                    end = f.reg(f.get_file.__next__())

            # assume md5sum of attribs[1] is lower, meaning that attribs[1] is now known
            is_1 = 1
            won = attribs[1]
            lost = attribs[0]
            if won.sum == lost.sum:
                # same signatre, so both are known now
                won.known = 1
                lost.known = 1
                if end:
                    break
                bsame += 1
                if cross == 'y':
                    cfp.write("{} - '{}' | '{}'\n".format(lost.sum, lost.name, won.name))
            else:
                if lost.sum < won.sum:
                    # assumtion was wrong, attribs[0] is lower
                    is_1 = 0
                    lost = won
                    won = attribs[0]
                won.known = 1
                lost.known = 0
                # Evaluate file on other disk based on its name
                res, s = eval_file(is_1, won.sum, won.name, lost.abs)
                res_char = '<' if is_1 else '>'
                if res == 0:
                    # There is an equal file on the other side
                    res_char = '='
                elif res == 1:
                    # There is a file with different content on the other side
                    res_char = '|'
                    bdiff += 1
                elif res < 0:
                    # this is a candidate for copying to the other side
                    won.add_size()
                    if is_1:
                        get_fp.write(won.name+"\n")
                    else:
                        put_fp.write(won.name+"\n")
                    won.files += 1
                if res < 2 and res != 0:
                    left_side = s
                    if is_1:
                        s = won.sum
                    else:
                        left_side = won.sum
                    rfp.write("{} {} {} {}\n".format(left_side, res_char, s, won.name))

        rfp.write("\n------------------------ Statistics Summary -----------------------\n")
        rfp.write("             Disk1               |              Disk2\n\n")
        rfp.write("{:>32s} >                                  bytes (files) should be copied from Disk1 to Disk2\n".format(
                            "{} ({})".format(attribs[0].accu_sum, attribs[0].files)))
        rfp.write("                                 < {:32s} (files) bytes should be copied from Disk2 to Disk1\n\n".format(
                            "({}) {}".format(attribs[1].files, attribs[1].accu_sum)))
        rfp.write("{:32d} | {:<32d} bytes total\n".format(attribs[0].tot, attribs[1].tot))
        rfp.write("{:32d} | {:<32d} unique files total\n".format(attribs[0].tot_uniq_files, attribs[1].tot_uniq_files))
        rfp.write("{:32d} | {:<32d} files with content which is present on both sides\n".format(bsame, bsame))
        rfp.write("{:32d} | {:<32d} named files have different content on other side, so, can't copy\n".format(bdiff, bdiff))
        rfp.write("{:32d} | {:<32d} bytes possible reduction due to duplicates\n".format(attribs[0].red, attribs[1].red))
        rfp.write("{:32d} | {:<32d} bytes size after copy and deleting duplicates\n".format(
                             attribs[0].tot - attribs[0].red + attribs[1].accu_sum, attribs[1].tot - attribs[1].red + attribs[0].accu_sum))
    attribs[0].get_file.close()
    attribs[1].get_file.close()

    os.chdir(out_dir)
    with open('sums', 'a') as mdfp:
        for name in ['sync', 'put', 'get']:
            mdfp.write(plain_md5(name)+"\n")
        if cross == 'y':
            mdfp.write(plain_md5('cross-duplicates')+"\n")
        else:
            os.unlink('cross-duplicates')


def update_status(file, text):
    '''
    Update running progress (status) into a signal file
    This is used from the main thread to display status to the user,
    and to wait for both threads which are checking the two disks
    in parallel to be finished.
    '''
    try:
        with open(file, 'wb') as f:
            f.write(text.encode('UTF-8'))
    except FileNotFoundError:
        pass


def make_md5_list(args):
    '''
    Produce the md5 list (signature list) of the disk
    Arguments:
       - Directory where the result files shall be stored
       - Number of files to check
       - Filename of file with the raw filenames of the disk (plain list)
       - Filename of file where running status (progress) should be reported
       - Absolute pointer to the disk folder
    '''
    hash_vals = []
    if len(args) < 2:
        print("ERROR: Too few arguments")  # to rep_file if exists
        return
    no_of_files = args[0]
    find_file = args[1]
    rep_file = args[2]
    target = args[3]
    out_dir = args[4]

    if not os.path.isfile(rep_file):
        print("ERROR: Signal file not found")  # to rep_file if exists
        return

    os.chdir(target)
    cnt = 0
    links_cnt = 0
    unknown_cnt = 0
    c = 0
    in_files_tot = int(no_of_files)
    pst_div = in_files_tot / 100.0
    if pst_div == 0:
        pst_div = 1.0
    fmt_text = '{:1.1f}%, {} files, {} links, {}G'
    size_done = 0

    with open(find_file, 'r') as fp, open(os.path.join(out_dir, 'md5'), 'w') as mdfp:
        line = fp.readline()
        cnt = 0
        c = 0
        while line:
            filename = line.strip()
            if os.path.islink(filename):
                sumline = "LINK 0 " + filename
                links_cnt += 1
            elif os.path.isfile(filename):
                if cnt % 50 == 0:
                    update_status(rep_file, fmt_text.format((cnt + links_cnt)/pst_div, cnt, links_cnt, size_done//1024//1024//1024))
                sumline = calc(filename)
                cnt += 1
            else:
                sumline = "UNKNOWN 0 " + filename
                unknown_cnt += 1
            c += 1
            msum, msize, mname = sumline.split(' ', 2)
            size_done += int(msize)
            strc = str(c)
            mdfp.write(msum+' '+msize+' '+chr(0x60 + len(strc))+strc+' '+mname+"\n")
            line = fp.readline()

    os.chdir(out_dir)
    with open('sums', 'w') as mdfp:
        mdfp.write(plain_md5('md5')+"\n")

    err_msg = ''
    if unknown_cnt != 0:
        err_msg = " warning: {} unknown names.".format(unknown_cnt)

    update_status(rep_file, fmt_text.format((cnt + links_cnt)/pst_div, cnt, links_cnt, size_done//1024//1024//1024) + err_msg + " done")
