#!/usr/bin/python3

import sys
from md5signature.md5signature import plain_md5

# Pal Saugstad, 2021-08-09 (pal@saugstad.net)

print(plain_md5(sys.argv[1]))
