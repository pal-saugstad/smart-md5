#!/bin/bash

scan='yes'
small_lim=10

[ -L $0 ] && SLOC=$(readlink $0) || SLOC=$0
LOC=$(realpath $(dirname $SLOC))
LOGDIR=/var/log/reg-smart-md5
LOG=${LOGDIR}/report.log

trap 'pkill -g $BGPID; exit' INT
BGPID=$$

#echo Pid is $BGPID
real_run='yes'

function log_from_thread {
  echo "$2" | sed "s/^/$(date --rfc-3339=seconds) $BGPID $1 - /g" >> $LOG
}


function find_signatures {
  dir=$1; shift
  targ=$1; shift
  rep=$1; shift
  abs=$1; shift
  echo -n start .. >$rep
  find_file=$(mktemp)
  echo "Scanning  $targ - $abs, results in: $dir";
  if [ ! -d "${abs}" ] ; then
    echo -n "ERROR: $abs is no folder" >$rep
    log_from_thread "$targ" "ERROR: $abs is no folder"
    return
  fi
  log_from_thread "$targ" "Scanning"
  cd "$abs"
  err_out=$(mktemp)
  for iter in 1 2 3 4 5 6 ; do
    echo -n '' >$err_out
    echo -n "scan # $iter .." >$rep
    find . -xtype f 2>$err_out >$find_file
    [ "$(cat $err_out)" ] || break
  done
  if [ "$(cat $err_out)" ] ; then
    echo "ERROR scanned $iter time without finding an errorfree list of files .." >$rep
    echo "ERROR is '$(cat $err_out)'" >$rep
    log_from_thread "$targ" "ERROR is '$(cat $err_out)'"
    rm $err_out
    return
  fi
  rm $err_out
  read -r files dmy <<<$(wc -l $find_file)
  log_from_thread "$targ" "Found $files files and links"
  ${LOC}/make-md5-list.py $files $find_file $rep "$abs" "$dir"
  if [ ! "$(cat $rep | grep ' done')" ] ; then
    log_from_thread "$targ" "Stopping thread: No 'done' in status: $(cat $rep)"
    return
  fi
  cd "$abs"
  log_from_thread "$targ" "Checking duplicates of files bigger than $small_lim KB"
  byte_lim=$((small_lim * 1024))
  duplicates=$(awk -v val="$byte_lim" '{if ($2>val) print $0}' <$dir/md5 | sort | uniq -dw32 | egrep '^[0-9a-f]{32}' | sort -k3)
  duplicates_file="${dir}/duplicates"
  echo -n '' > $duplicates_file
  no_of_files_with_dups=$(echo "$duplicates" | grep '[0-9a-f]' | wc -l)
  if [ $no_of_files_with_dups -gt 0 ] ; then
    tot_uniq=$(cat "${dir}/md5" | sort | uniq -w32 | wc -l)
    log_from_thread "$targ" "$no_of_files_with_dups of $tot_uniq unique files bigger than $small_lim KB have duplicates"
    cnt=0
    keep=0

    while read -r sum dmy; do
      [ "$sum" ] || continue
      group=$(grep $sum $dir/md5)
      keep=$((keep+1))
      echo "GROUP $keep" >> "$duplicates_file"
      cnt=$((cnt + $(echo "$group" | wc -l) - 1))
      echo "$group" >> "$duplicates_file"
    done <<< "$duplicates"
    echo "GROUP end" >> "$duplicates_file"
    log_from_thread "$targ" "$cnt files (duplicates) can be deleted."
  else
    log_from_thread "$targ" "No files bigger than $small_lim KB has duplicates"
  fi
  cd ${dir}
  ${LOC}/plain-md5.py "duplicates" >> "sums"
  ${LOC}/plain-md5.py "disk" >> "sums"
  echo -n " DONE" >> $rep
  log_from_thread "$targ" "Thread finished: Status: $(cat $rep)"
}

function copy_files {
  list=$1
  from=$2
  from_dir=$3
  to=$4
  to_dir=$5
  cd "$to_dir"
  no_of_files=$(cat "$list" | wc -l)
  cnt=0
  if [ $no_of_files -gt 0 ] ; then
    while read -r file ; do
      cnt=$((cnt+100))
      echo -n " Copy $no_of_files files from $from to $to: $((cnt/no_of_files))% ..."$'\r'
      [ "$file" ] || continue
      folder=$(dirname "$file")
      mkdir -p "${folder}"
      cp -a "${from_dir}/$file" "${folder}"
    done < "$list"
    echo
    log_from_thread "| common" "Done copying $no_of_files files from $from to $to"
  fi

}

function wait_for_disks_to_be_scanned {
  while : ; do
    sleep 1
    read_disk1_rep=$(cat $disk1_rep)
    read_disk2_rep=$(cat $disk2_rep)
    [ "$1" != "Disk2" ] && echo -n " Disk1: $read_disk1_rep."
    [ "$1" != "Disk1" -a "$abs_disk2" ] && echo -n " Disk2: $read_disk2_rep."
    echo -n $'\r'
    if [ "$(echo $read_disk1_rep | grep DONE)" -o "$1" == "Disk2" ] ; then
      [ "$(echo $read_disk2_rep | grep DONE)" -o "$1" == "Disk1" ] && break
      [ "$abs_disk2" ] || break
    fi
    if [ "$(echo ${read_disk2_rep} ${read_disk1_rep} | grep ERROR)" ] ; then
      echo -n $'\n'$'\n'"Error during scan ... "
      rm $disk1_rep
      rm $disk2_rep
      pkill -g $BGPID
      exit
    fi
  done
  echo
}

function reg_disk {
  echo "$1" > ${LOGDIR}/disk
  id=$(md5sum ${LOGDIR}/disk | cut -c1-5)
  mkdir -p ${LOGDIR}/$id
  mv ${LOGDIR}/disk ${LOGDIR}/$id
  echo "${LOGDIR}/$id"
}

# parse input
abs_disk1=''
abs_disk2=''
all=''
option=''
copy_left=''
copy_right=''
cross='n'
while [ "$1" ] ; do
  param=$1
  if [  "$option" == "s" ] ; then
    small_lim=$((param * 1))
    echo "Don't regard files smaller than $small_lim KB when searching for duplicates"
    option=''
  elif [ "${param::1}" == "-" ] ; then
    while : ; do
      param=${param:1:1000}
      [ "$param" ] || break
      option=${param::1}
      if [ "$option" == "l" ] ; then
        copy_left='y'
        echo "Copy files to the left,  Disk2 to Disk1"
      elif [ "$option" == "r" ] ; then
        copy_right='y'
        echo "Copy files to the right, Disk1 to Disk2"
      elif [ "$option" == "o" ] ; then
        scan=''
        echo "Omit initial scan (Note: Use with care!)"
      elif [ "$option" == "x" ] ; then
        cross='y'
        echo "Make a cross-duplicate list"
      elif [ "$option" == "s" ] ; then
        :
      else
        echo "Unknown option $option"
        exit
      fi
    done
  else
    if [ ! "$abs_disk1" ] ; then
      abs_disk1="$(realpath "$1")"
      echo "Disk1 = '$abs_disk1'"
    elif [ ! "$abs_disk2" ] ; then
      abs_disk2="$(realpath "$1")"
      echo "Disk2 = '$abs_disk2'"
    fi
  fi
  shift
done

[ $(stat -c%s $LOG) -gt 100000 ] && mv $LOG $LOG.1

echo
echo "reg-smart-md5 - find duplicates and copy based on signatures (file content) (PID $BGPID)"

came_from=$(pwd)

if [ ! "$abs_disk1" ] ; then
  cat <<-HELPTEXT
Parameters: [<options>] <Disk1> [<Disk2>]
If <Disk2> not specified: Just calculate signatures of Disk1

Note: <Diskn> don't have to be actual Disks, they can be differnet folders on the same or different disks too.

Options:
 No options: Just report properties of the disk(s), don't alter anything
  -r       : Copy files to the right, Disk1 to Disk2
  -l       : Copy files to the left,  Disk2 to Disk1
  -x       : Make cross-duplicates, showing all content which is stored on both disks. Useful if you want to isolate files on each disk.
  -o       : Omit initial scan, rather assume the disk files are consistent with the content of previous scan (Note: Use with care!)
  -s val   : Don't regard files smaller than <val> KB when searching for duplicates (Default 10 KB)

Purpose:
 - Find duplicated files and group them together in the report file 'duplicates'.
 - Find unique files based on content and copy them over to the other disk (based on options l and r).
   Report files (with same name) which are different on the disks. The program will never copy such files.
   Do a manual check to see which of them are damaged and take manual action before proceeding.

Report files:
   For each disk, an 'encoded' directory in the folder ${LOGDIR} will be made.
   The ${LOGDIR}/<HASH> directory will contain report files after the run:

   For both disks:
      md5        : The signatures of all the files
      duplicates : Groups of files of this disk which has the same content
      sums       : md5sums of the current system files in the ${LOGDIR}/<HASH> folder (these files)

   For Disk1 only, when two disks are scanned:
      sync       : Report of which files is missing on the other disk,
                   which files with same name are different on the two disks and a report summary
      put        : The list of files to copy to the other disk in order to be in sync (see -r option)
      get        : The list of files to copy from the other disk in order to be in sync (see -l option)

   Log file:
      Activity is logged to the file $LOG

HELPTEXT

   exit
elif [ ! -d "$abs_disk1" ] ; then
   echo "Disk1 '$abs_disk1' is not a directory"
   exit
elif [ "$abs_disk2" ] ; then
  if [ ! -d "$abs_disk2" ] ; then
     echo "Disk2 '$abs_disk2' is not a directory"
     exit
  fi
fi
log_from_thread "< Disk1 " "$abs_disk1"
dir1=$(reg_disk "$abs_disk1")
dir2=''
if [ "$abs_disk2" ] ; then
  if [ "$abs_disk1" == "$abs_disk2" ] ; then
    log_from_thread "> Disk2 " "$abs_disk2, same as Disk1, abort"
    echo "Disk1 and Disk2 must be different directories"
    exit
  fi
  log_from_thread "> Disk2 " "$abs_disk2"
  dir2=$(reg_disk "$abs_disk2")
fi
echo
log_from_thread "       -" "-"

if [ "$scan" ] ; then
  disk1_rep=$(mktemp)
  disk2_rep=$(mktemp)
  find_signatures "$dir1" "< Disk1 " "$disk1_rep" "$abs_disk1" &
  dev1=$(df "$abs_disk1" | cut -d' ' -f1)
  dev2=''
  sleep 0.1
  if [ "$abs_disk2" ] ; then
    dev2=$(df "$abs_disk2" | cut -d' ' -f1)
    [ "$dev1" == "$dev2" ] && wait_for_disks_to_be_scanned Disk1
    find_signatures "$dir2" "> Disk2 " "$disk2_rep" "$abs_disk2" &
    [ "$dev1" == "$dev2" ] && wait_for_disks_to_be_scanned Disk2
  fi
  sleep 0.1
  read_disk1_rep=''
  read_disk2_rep=''
  [ "$dev1" == "$dev2" ] || wait_for_disks_to_be_scanned ''
  rm $disk1_rep
  rm $disk2_rep
fi
echo $'\n'Done scanning
log_from_thread "| common" "Done scanning"

if [ "$abs_disk2" ] ; then
  log_from_thread "| common" "Start statistics"

  f_sort=$(mktemp)
  sort "$dir1/md5" >$f_sort
  t_sort=$(mktemp)
  sort "$dir2/md5" >$t_sort
  ${LOC}/check-sync.py "$dir1" $cross $small_lim "$abs_disk1" $f_sort "$abs_disk2" $t_sort
  rm $f_sort
  rm $t_sort
  log_from_thread "| common" "Done statistics"
  grep ' Statistics Summary ' -A 16 "$dir1/sync"
  log_from_thread "< Disk1 " "$(grep ' Statistics Summary ' -A 16 "$dir1/sync")"
fi
cd "$dir1"
log_from_thread "< Disk1 " "------ Report files for Disk1, '$abs_disk1' in '$dir1':"
log_from_thread "< Disk1 " "$(md5sum -c sums)"

if [ "$abs_disk2" ] ; then
  cd "$dir2"
  log_from_thread "> Disk2 " "------ Report files for Disk2, '$abs_disk2' in '$dir2':"
  log_from_thread "> Disk2 " "$(md5sum -c sums)"
  [ "$copy_right" ] && copy_files "$dir1/put" "Disk1" "$abs_disk1" "Disk2" "$abs_disk2"
  [ "$copy_left"  ] && copy_files "$dir1/get" "Disk2" "$abs_disk2" "Disk1" "$abs_disk1"
fi
cd "$came_from"
echo "See full report in file $LOG"
