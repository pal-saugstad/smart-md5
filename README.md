
# Smart MD5

OS: Linux

Languages: Bash script and Python3

Checking and syncing big disks with lots of content using the content as key for comparing files, not filenames.


## Problem

Big disks (> 1TB) with big files (> 1GB) (Video, Audio, Photos) must be kept tidy, and there should be one or more backups.

- I don't want there to be many copies of the same item on my source disk.
- I want to make backups based on the content of the files on the source disk.
- I don't want to copy a file from the source disk directly over to the backup if they both already exist and they have different content, since the reason for them being different could be that the source file is damaged.
- If items have been moved or deleted on the source disk in order to make it more user friendly, the sync/backup routine should noify me so that I can do the same with the backup before syncing.
- So, the backup process should only copy files which are not stored on the backup from before, and notify any other irregularities.


## How to do it

Produce md5-hashes of all the files on both disks and compare the hashes, and find duplicates on each disk.
In order to speed up the md5-hash production, I don't scan the entire file if it is big.
Instead, just 3 blocks of 1 MB of the big files are scanned: At the start, in the middle and at the end.
This means that the hash value could be the same even if the files are different,
but if so, that would be because the file is damaged rather than those files actually being differnt content.


# Current status and behavior

Help text:

```
$ reg-smart-md5.sh

Parameters: [<options>] <Disk1> [<Disk2>]
If <Disk2> not specified: Just calculate signatures of Disk1

Note: <Diskn> don't have to be actual Disks, they can be differnet folders on the same or different disks too.

Options:
 No options: Just report properties of the disk(s), don't alter anything
  -r       : Copy files to the right, Disk1 to Disk2
  -l       : Copy files to the left,  Disk2 to Disk1
  -o       : Omit initial scan, rather assume the disk files are consistent with the content of .smart-md5/md5 (Note: Use with care!)
  -s val   : Don't regard files smaller than <val> KB when searching for duplicates (Default 10 KB)

Purpose:
 - Find duplicated files and group them together in the report file 'duplicates'.
 - Find unique files based on content and copy them over to the other disk (based on options l and r).
   Report files (with same name) which are different on the disks. The program will never copy such files.
   Do a manual check to see which of them are damaged and take manual action before proceeding.

Report files:
   For each disk, a hidden directory '.smart-md5' in the root folder pointed to by <Diskn> will be made.
   The '.smart-md5' directory will contain report files after the run:

   On both disks, representing that disk:
      md5        : The signatures of all the files
      duplicates : Groups of files of this disk which has the same content
      sums       : md5sums of the current system files in the .smart-md5 folder (these files)

   On Disk1 only, when two disks are scanned:
      sync       : Report of which files is missing on the other disk,
                   which files with same name are different on the two disks and a report summary
      put        : The list of files to copy to the other disk in order to be in sync (see -r option)
      get        : The list of files to copy from the other disk in order to be in sync (see -l option)

   Log file:
      Activity is logged to the file /var/log/reg-smart-md5/report.log
```

Log example:

```
/media/pals  $ cat /var/log/reg-smart-md5/report.log
2021-08-19 13:33:43+02:00 79932 < Disk1  - /media/pals/Media-psv/media
2021-08-19 13:33:43+02:00 79932 > Disk2  - /media/pals/One Touch/media
2021-08-19 13:33:43+02:00 79932        - - -
2021-08-19 13:33:43+02:00 79932 < Disk1  - Scanning
2021-08-19 13:33:43+02:00 79932 > Disk2  - Scanning
2021-08-19 13:33:44+02:00 79932 < Disk1  - Found 44076 files and links when disregarding system files in folders '.smart-md5/'
2021-08-19 13:33:44+02:00 79932 > Disk2  - Found 46224 files and links when disregarding system files in folders '.smart-md5/'
2021-08-19 13:48:53+02:00 79932 > Disk2  - Checking duplicates of files bigger than 10 KB
2021-08-19 13:48:53+02:00 79932 > Disk2  - 2049 of 43489 unique files bigger than 10 KB have duplicates
2021-08-19 13:49:06+02:00 79932 > Disk2  - 2083 files (duplicates) can be deleted.
2021-08-19 13:49:07+02:00 79932 > Disk2  - Thread finished: Status: 100.0%, 46224 files, 0 links, 1502G done DONE
2021-08-19 13:49:13+02:00 79932 < Disk1  - Checking duplicates of files bigger than 10 KB
2021-08-19 13:49:14+02:00 79932 < Disk1  - 216 of 43390 unique files bigger than 10 KB have duplicates
2021-08-19 13:49:15+02:00 79932 < Disk1  - 216 files (duplicates) can be deleted.
2021-08-19 13:49:15+02:00 79932 < Disk1  - Thread finished: Status: 100.0%, 44076 files, 0 links, 1484G done DONE
2021-08-19 13:49:16+02:00 79932 | common - Done scanning
2021-08-19 13:49:16+02:00 79932 | common - Start statistics
2021-08-19 13:49:17+02:00 79932 | common - Done statistics
2021-08-19 13:49:17+02:00 79932 < Disk1  - ------------------------ Statistics Summary -----------------------
2021-08-19 13:49:17+02:00 79932 < Disk1  -              Disk1               |              Disk2
2021-08-19 13:49:17+02:00 79932 < Disk1  -
2021-08-19 13:49:17+02:00 79932 < Disk1  -                            0 (0) >                                  bytes (files) should be copied from Disk1 to Disk2
2021-08-19 13:49:17+02:00 79932 < Disk1  -                                  < (98) 266606904                   (files) bytes should be copied from Disk2 to Disk1
2021-08-19 13:49:17+02:00 79932 < Disk1  -
2021-08-19 13:49:17+02:00 79932 < Disk1  -                    1593773879173 | 1613054189119                    bytes total
2021-08-19 13:49:17+02:00 79932 < Disk1  -                                2 | 2                                named files have different content on other side, so, can't copy
2021-08-19 13:49:17+02:00 79932 < Disk1  -                       1308948909 | 20322152904                      bytes possible reduction due to duplicates
2021-08-19 13:49:17+02:00 79932 < Disk1  -                    1592731537168 | 1592732036215                    bytes size after copy and deleting duplicates
2021-08-19 13:49:17+02:00 79932 < Disk1  - ------ Report files for Disk1 in '/media/pals/Media-psv/media':
2021-08-19 13:49:17+02:00 79932 < Disk1  - .smart-md5/md5: OK
2021-08-19 13:49:17+02:00 79932 < Disk1  - .smart-md5/duplicates: OK
2021-08-19 13:49:17+02:00 79932 < Disk1  - .smart-md5/sync: OK
2021-08-19 13:49:17+02:00 79932 < Disk1  - .smart-md5/put: OK
2021-08-19 13:49:17+02:00 79932 < Disk1  - .smart-md5/get: OK
2021-08-19 13:49:17+02:00 79932 > Disk2  - ------ Report files for Disk2 in '/media/pals/One Touch/media':
2021-08-19 13:49:17+02:00 79932 > Disk2  - .smart-md5/md5: OK
2021-08-19 13:49:17+02:00 79932 > Disk2  - .smart-md5/duplicates: OK
```
