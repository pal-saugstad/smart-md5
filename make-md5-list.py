#!/usr/bin/python3

import sys
from md5signature.md5signature import make_md5_list

# Pal Saugstad, 2021-08-09 (pal@saugstad.net)

sys.argv.pop(0)
make_md5_list(sys.argv)
