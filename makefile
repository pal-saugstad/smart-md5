SHARE=/usr/share/python3
HERE=$(shell pwd)
PROJ=$(shell basename ${HERE})
NAME=reg-smart-md5.sh
START=/usr/bin/${NAME}

install: syntax files ${START}
	echo Done

syntax:
	pep8 --first --max-line-length=150 .

files:
	sudo rsync -ai --delete --exclude='.git/' ${HERE} ${SHARE}

${START}:
	sudo ln -s ${SHARE}/${PROJ}/${NAME} ${START}

uninstall: uninst_share uninst_link

uninst_link:
	sudo rm ${START}

uninst_share:
	sudo rm -r ${SHARE}/${PROJ}
